import { PaletteMode } from "@mui/material";
import { green } from "@mui/material/colors";

export const getDesignTokens = (mode: PaletteMode) => ({
  palette: {
    mode,
    ...(mode === "light" ? lightModePalette : darkModePalette),
  },
  typography: { fontFamily: "cursive" },
});

const lightModePalette = {
  primary: green,
  divider: "#cae9d2",
  background: {
    default: "#cae9d2",
    secondary: "#fff",
    paper: "#2a945b",
  },
  text: {
    primary: "#2a945b",
    secondary: "#fff",
  },
};

const darkModePalette = {
  primary: green,
  divider: "#2a945b",
  background: {
    default: "#2a945b",
    secondary: "#4caf50",
    paper: "#cae9d2",
  },
  text: {
    primary: "#fff",
    secondary: "#000",
  },
};
