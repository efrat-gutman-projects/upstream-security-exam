import { Button, styled } from "@mui/material";

export const CustomizedButton = styled(Button)(
    ({ theme }) => ({
        color: `${theme.palette.text.secondary}`,
        backgroundColor:`${theme.palette.background.paper}`,
        margin: "1em",
        display: "block",
        fontWeight: "bold",
        "&:hover": {
            backgroundColor: `${theme.palette.background.default}`,
          },
      }),
  );