import React from "react";
import { Box, Typography } from "@mui/material";

export const HomePage = (): JSX.Element => {
  return (
    <Box sx={{ display: "flex", justifyContent: "center", marginTop: "10%" }}>
      <Typography variant="h4" component="div" sx={{ fontWeight: "bold" }}>
        welcome to our restaurant reservation system
      </Typography>
    </Box>
  );
};
