import React, { useContext } from "react";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import IconButton from '@mui/material/IconButton';
import LightIcon from "@mui/icons-material/Brightness4";
import DarkIcon from "@mui/icons-material/Brightness7";
import { useTheme } from '@mui/material/styles';
import { Link } from "react-router-dom";

import { ColorModeContext } from "../../store";
import { CustomizedButton } from "../../shared";

export const NavBar = (): JSX.Element => {
  const pages = [
    { name: "Home", path: "/" },
    { name: "Reservations", path: "/reservations" },
  ];

  const colorMode = useContext(ColorModeContext);
  const { palette } = useTheme();

  const displayPagesButton = (): JSX.Element[] =>
    pages.map(({ name, path }) => (
      <CustomizedButton key={name}>
        <Link
          to={path}
          style={{ textDecoration: "none", margin: "1rem", color: "#fff" }}
        >
          {name}
        </Link>
      </CustomizedButton>
    ));

  return (
    <>
      <AppBar position="sticky" sx={{ bgcolor: "background.secondary" }}>
        <Toolbar>
          <Typography
            variant="h4"
            component="div"
            sx={{ color: "text.primary", flexGrow: 1 }}
          >
            Restaurant Reservations
          </Typography>
          {displayPagesButton()}
          <IconButton sx={{ ml: 1 }} onClick={colorMode.toggleColorMode}>
            {palette.mode === "dark" ? <DarkIcon /> : <LightIcon />}
          </IconButton>
        </Toolbar>
      </AppBar>
    </>
  );
};
