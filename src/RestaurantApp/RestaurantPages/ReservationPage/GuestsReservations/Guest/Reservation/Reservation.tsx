import React, { Dispatch, SetStateAction, useState } from "react";
import Box from "@mui/material/Box";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import { SelectChangeEvent } from "@mui/material";

import { mealOptions } from "./MealOptions";

type ReservationProps = {
  onReservationReady: Dispatch<SetStateAction<boolean>>;
};

export const Reservation = (props: ReservationProps): JSX.Element => {
  const { onReservationReady } = props;
  const numOfCourse = 3;

  const [meals, setMeals] = useState<string[]>([""]);

  const handleOnChangeMeal =
    (courseIndex: number) => (event: SelectChangeEvent) => {
      const resetNextCourses = meals.slice(0, courseIndex);
      const updatedMeals = resetNextCourses.concat([event.target.value]);
      const isLastCourse = courseIndex + 1 === numOfCourse;

      if (isLastCourse) {
        onReservationReady(true);
        setMeals(updatedMeals);
      } else {
        onReservationReady(false);
        setMeals([...updatedMeals, ""]);
      }
    };

  return (
    <>
      {meals.map((meal, index) => (
        <Box sx={{ minWidth: 165, marginLeft: "1em" }} key={index}>
          <FormControl fullWidth>
            <InputLabel>Select Course {index + 1}</InputLabel>
            <Select
              value={meal}
              label="Select Course"
              onChange={handleOnChangeMeal(index)}
            >
              {mealOptions[index].map((option, index) => (
                <MenuItem
                  key={index}
                  value={`${option}`}
                  sx={{ color: "text.secondary" }}
                >
                  {option}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Box>
      ))}
    </>
  );
};
