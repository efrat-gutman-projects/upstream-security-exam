import React, { useState, useEffect } from "react";
import { Box, Typography } from "@mui/material";

import { Reservation } from "./Reservation";
import { CustomizedButton } from "../../../../shared";

type GuestProps = {
  guestId: string;
  guestNumber: number;
  onDeleteGuest: (id: string) => void;
  onGuestReady: (isReady: boolean, guestNumber: number) => void;
};

export const Guest = (props: GuestProps): JSX.Element => {
  const { guestId, onDeleteGuest, onGuestReady, guestNumber } = props;

  const [isReservationReady, setIsReservationReady] = useState(false);

  const handleOnDeleteGuest = () => {
    onDeleteGuest(guestId);
  };

  useEffect(() => {
    onGuestReady(isReservationReady, guestNumber);
  }, [isReservationReady]);

  return (
    <Box sx={{ display: "flex", whiteSpace: "nowrap", marginTop: "5%" }}>
      <Typography variant="h6" component="div" marginTop="0.5em">
        {`Guest ${guestNumber}`}
      </Typography>
      <Reservation onReservationReady={setIsReservationReady} />
      {isReservationReady && (
        <CustomizedButton onClick={handleOnDeleteGuest}>
          Delete Guest
        </CustomizedButton>
      )}
    </Box>
  );
};
