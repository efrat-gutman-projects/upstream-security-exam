import React, { useState } from "react";
import { Box } from "@mui/material";

import { Guest } from "./Guest";
import { CustomizedButton } from "../../../shared";

export const GuestsReservations = (): JSX.Element => {
  const [guests, setGuests] = useState<string[]>([]);
  const [isGuestCanBeAdded, setIsGuestCanBeAdded] = useState<boolean>(true);

  const handleOnAddGuest = () => {
    setGuests([...guests, `${Math.random() * 100}`]);
    setIsGuestCanBeAdded(false);
  };

  const handleOnDeleteGuest = (GuestId: string) => {
    const restGuests = guests.filter((guest) => guest !== GuestId);

    setGuests(restGuests);
  };

  const handleOnGuestReady = (isReady: boolean, guestNumber: number) => {
    const isLastGuest = guests.length === guestNumber;

    isLastGuest && setIsGuestCanBeAdded(isReady) ;
  };

  return (
    <>
      {guests.map((guest, index) => (
        <Box key={guest} sx={{ display: "flex", marginLeft: "20%" }}>
          <Guest
            guestNumber={index + 1}
            guestId={guest}
            onDeleteGuest={handleOnDeleteGuest}
            onGuestReady={handleOnGuestReady}
          />
        </Box>
      ))}
      {isGuestCanBeAdded && (
        <CustomizedButton sx={{ left: "43%" }} onClick={handleOnAddGuest}>
          Add Guest
        </CustomizedButton>
      )}
    </>
  );
};
