import React from "react";
import { Typography } from "@mui/material";
import { useNavigate } from "react-router-dom";

import { CustomizedButton } from "../../shared";
import { GuestsReservations } from "./GuestsReservations";

export const ReservationPage = (): JSX.Element => {
  const navigate = useNavigate();

  const handleOnGoBack = () => {
    navigate(-1);
  };

  return (
    <>
      <CustomizedButton onClick={handleOnGoBack}>Go back</CustomizedButton>
      <Typography variant="h4" component="div" align="center">
        My Guests Reservations:
      </Typography>
      <GuestsReservations />
    </>
  );
};
