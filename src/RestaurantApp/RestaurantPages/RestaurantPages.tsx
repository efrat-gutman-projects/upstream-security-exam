import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Box } from "@mui/system";

import { NavBar } from "./NavBar";
import { ReservationPage } from "./ReservationPage";
import { HomePage } from "./HomePage";

export const RestaurantPages = (): JSX.Element => {
  return (
    <Box
      sx={{
        height: "100vh",
        bgcolor: "background.default",
        color: "text.primary",
        overflow: "scroll",
      }}
    >
      <BrowserRouter>
        <NavBar />
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/reservations" element={<ReservationPage />} />
        </Routes>
      </BrowserRouter>
    </Box>
  );
};
