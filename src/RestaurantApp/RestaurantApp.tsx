import React, { useMemo, useState } from "react";
import { ThemeProvider, createTheme } from "@mui/material/styles";

import { RestaurantPages } from "./RestaurantPages";
import { ColorModeContext } from "./store";
import { getDesignTokens } from "./style";

const RestaurantApp = (): JSX.Element => {
  const [mode, setMode] = useState<"light" | "dark">("light");

  const setColorMode = () => {
    setMode((prevMode) => (prevMode === "light" ? "dark" : "light"));
  };

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const colorMode = useMemo(() => ({ toggleColorMode: setColorMode }), []);
  const theme = useMemo(() => createTheme(getDesignTokens(mode)), [mode]);

  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <RestaurantPages />
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
};

export default RestaurantApp;
