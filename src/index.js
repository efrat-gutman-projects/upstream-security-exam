import ReactDOM from 'react-dom/client';

import RestauratApp from './RestaurantApp';

import './index.css';

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(<RestauratApp />);